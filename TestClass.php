<?php

declare(strict_types = 1);

namespace App\TestTask;

use App\TestTask\ClassesDomainLayer\Author;
use App\TestTask\ClassesDomainLayer\Book;
use App\TestTask\ClassesDomainLayer\Dto\LicensePaymentRequestDto;
use App\TestTask\ClassesDomainLayer\Moderator;
use App\TestTask\ClassesDomainLayer\User;
use App\TestTask\ClassesDomainLayer\ValueObject\CurrencyType;
use App\TestTask\NotificationSystem\Dto\SystemDataDto;
use App\TestTask\NotificationSystem\EmailNotification\EmailNotificationService;
use App\TestTask\NotificationSystem\EmailNotification\Notification\AuthorizationFraudEmailNotification;
use App\TestTask\NotificationSystem\EmailNotification\Notification\LicensePaymentRequestAdminEmailNotification;
use App\TestTask\NotificationSystem\EmailNotification\Notification\LicensePaymentRequestAuthorEmailNotification;
use App\TestTask\NotificationSystem\NotificationServiceInterface;
use App\TestTask\NotificationSystem\SystemNotification\Notification\AuthorBlockBookSystemNotification;
use App\TestTask\NotificationSystem\SystemNotification\SystemNotificationService;

class TestClass
{
    /** @var SystemNotificationService */
    private $systemNotificationService;

    /** @var EmailNotificationService */
    private $emailNotificationService;

    public function __construct(
        SystemNotificationService $systemNotificationService,
        EmailNotificationService $emailNotificationService
    ) {
        $this->systemNotificationService = $systemNotificationService;
        $this->emailNotificationService  = $emailNotificationService;
    }

    public function runEvents(): void
    {
        $moderatorId = 1;
        $authorId    = 28;
        $bookId      = 4853;
        $userId      = 7411;
        $fraudUserIp = '125.254.484.514';
        $comment     = 'Reason comment';
        $requestSum  = 14521.12;

        $moderator = Moderator::findOne($moderatorId);
        $author    = Author::findOne($authorId);
        $book      = Book::findOne($bookId);
        $user      = User::findOne($userId);

        $this->blockBookEvent($this->systemNotificationService, $moderator, $author, $book, $comment);
        $this->authorizationFraudEvent($this->emailNotificationService, $user, $fraudUserIp);
        $this->licensePaymentRequestEvent($this->emailNotificationService, $moderator, $author, $requestSum);
    }

    private function blockBookEvent(
        NotificationServiceInterface $service,
        Moderator $moderator,
        Author $author,
        Book $book,
        string $comment
    ): void {
        $systemDataDto = (new SystemDataDto())
            ->setIp('187.251.584.145')
            ->setDateTime(new \DateTimeImmutable())
            ->setUserAgent('Software Types: Web Browser');

        $notification = new AuthorBlockBookSystemNotification($moderator, $author, $book, $comment);

        $service
            ->setSystemData($systemDataDto)
            ->addNotification($notification);
    }

    private function authorizationFraudEvent(
        NotificationServiceInterface $service,
        User $user,
        string $fraudUserIp
    ): void {
        $systemDataDto = (new SystemDataDto())
            ->setIp('187.251.584.145')
            ->setDateTime(new \DateTimeImmutable())
            ->setUserAgent('Software Types: Web Browser');

        $notification = new AuthorizationFraudEmailNotification($user, $fraudUserIp);

        $service
            ->setSystemData($systemDataDto)
            ->addNotification($notification);
    }

    private function licensePaymentRequestEvent(
        NotificationServiceInterface $service,
        Moderator $moderator,
        Author $author,
        float $requestSum
    ): void {
        $licensePaymentRequestDto = (new LicensePaymentRequestDto())
            ->setAuthorId($author->getId())
            ->setCurrency(CurrencyType::USD)
            ->setSum($requestSum)
            ->setRequisite($author->requisite());

        $systemDataDto = (new SystemDataDto())
            ->setIp('187.251.584.145')
            ->setDateTime(new \DateTimeImmutable())
            ->setUserAgent('Software Types: Web Browser');

        $adminNotification = new LicensePaymentRequestAdminEmailNotification(
            $moderator,
            $author,
            $licensePaymentRequestDto
        );

        $authorNotification = new LicensePaymentRequestAuthorEmailNotification(
            $moderator,
            $author,
            $licensePaymentRequestDto
        );

        $service
            ->setSystemData($systemDataDto)
            ->addNotification($adminNotification);

        $service
            ->setSystemData($systemDataDto)
            ->addNotification($authorNotification);
    }
}
