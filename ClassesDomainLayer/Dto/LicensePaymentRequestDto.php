<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer\Dto;

class LicensePaymentRequestDto
{
    /** @var int */
    private $authorId;

    /** @var float */
    private $sum;

    /** @var string */
    private $currency;

    /** @var array */
    private $requisite;

    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    public function setAuthorId(int $authorId): self
    {
        $this->authorId = $authorId;

        return $this;
    }

    public function getSum(): float
    {
        return $this->sum;
    }

    public function setSum(float $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getRequisite(): array
    {
        return $this->requisite;
    }

    public function setRequisite(array $requisite): self
    {
        $this->requisite = $requisite;

        return $this;
    }
}
