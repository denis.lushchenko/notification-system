<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class Book
{
    /** @var int */
    private $id;

    public static function findOne(int $id): Book
    {
        return (new Book())
            ->setId($id);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }
}
