<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer\ValueObject;

class CurrencyType
{
    public const USD = 'usd';
}
