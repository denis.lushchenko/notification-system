<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class Moderator
{
    /** @var int */
    private $id;

    public static function findOne(int $id): Moderator
    {
        return (new Moderator())
            ->setId($id);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return 'moderator@test.com';
    }
}
