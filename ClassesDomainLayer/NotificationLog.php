<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class NotificationLog
{
    public static function save(
        int $notificationType,
        string $ip,
        \DateTimeImmutable $dateTime,
        string $userAgent,
        array $additionData
    ) {
        $additionData = json_encode($additionData);
        echo(sprintf(
            "Saved NotificationLog with value: \nNotificationType = %d, ip = %s, dateTime = %s, userAgent = %s, additionData = %s\n",
            $notificationType,
            $ip,
            $dateTime->format('Y-m-d H:i:s'),
            $userAgent,
            $additionData
        ));
    }
}
