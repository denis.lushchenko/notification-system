<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class SystemService
{
    public function notify(string $systemMessageText, int $systemRecipient, array $systemData): void
    {
        $systemData = json_encode($systemData);
        echo(sprintf(
            "Send SystemService Notification with values: \nSystemMessageText = %s, SystemRecipient = %d, SystemData = %s\n",
            $systemMessageText,
            $systemRecipient,
            $systemData,
        ));
    }
}
