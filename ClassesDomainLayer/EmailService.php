<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class EmailService
{
    public function notify(string $emailRecipient, string $emailTemplate, array $emailData): void
    {
        $emailData = json_encode($emailData);
        echo(sprintf(
            "Send email with values: \nEmailRecipient = %s, EmailTemplate = %s, EmailData = %s\n",
            $emailRecipient,
            $emailTemplate,
            $emailData,
        ));
    }
}
