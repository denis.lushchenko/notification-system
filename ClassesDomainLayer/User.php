<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class User
{
    /** @var int */
    private $id;

    public static function findOne(int $id): User
    {
        return (new User())
            ->setId($id);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return 'user@user.com';
    }

    public function getName(): string
    {
        return 'Username';
    }
}
