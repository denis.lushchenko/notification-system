<?php

declare(strict_types = 1);

namespace App\TestTask\ClassesDomainLayer;

class Author
{
    /** @var int */
    private $id;

    public static function findOne(int $id): Author
    {
        return (new Author())
            ->setId($id);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function requisite(): array
    {
        return [
            'City'   => 'City',
            'Bank'   => 'Bank',
            'number' => '2465415244532432',
        ];
    }

    public function getEmail()
    {

        return 'author@test.com';
    }
}
