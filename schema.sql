CREATE TABLE IF NOT EXISTS `notificationLog` (
  `id`             INT         UNSIGNED NOT NULL AUTO_INCREMENT,
  `type`           SMALLINT    UNSIGNED NOT NULL,
  `ip`             VARCHAR(20)          NOT NULL,
  `date`           TIMESTAMP            NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userAgent`      TEXT                 NOT NULL,
  `additionalData` TEXT                 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = `utf8`;
