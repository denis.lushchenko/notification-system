<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem;

use App\TestTask\ClassesDomainLayer\NotificationLog;
use App\TestTask\NotificationSystem\Dto\SystemDataDto;

abstract class AbstractNotificationService implements NotificationServiceInterface
{
    /** @var SystemDataDto */
    private $systemDataDto;

    public function setSystemData(SystemDataDto $systemDataDto): self
    {
        $this->systemDataDto = $systemDataDto;

        return $this;
    }

    protected function saveNotification(NotificationInterface $notification): void
    {
        NotificationLog::save(
            $notification->getType(),
            $this->systemDataDto->getIp(),
            $this->systemDataDto->getDateTime(),
            $this->systemDataDto->getUserAgent(),
            $notification->getAdditionalData(),
            );
    }
}
