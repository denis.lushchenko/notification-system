<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem;

interface NotificationServiceInterface
{
    public function addNotification(NotificationInterface $notification): void;
}
