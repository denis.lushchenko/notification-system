<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem;

interface NotificationInterface
{
    public function getType(): int;

    public function getAdditionalData(): array;
}
