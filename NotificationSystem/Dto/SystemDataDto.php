<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\Dto;

class SystemDataDto
{
    /** @var string */
    private $ip;

    /** @var \DateTimeImmutable */
    private $dateTime;

    /** @var string */
    private $userAgent;

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDateTime(): \DateTimeImmutable
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeImmutable $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }
}
