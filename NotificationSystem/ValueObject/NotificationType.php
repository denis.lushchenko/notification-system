<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\ValueObject;

class NotificationType
{
    public const AUTHOR_BLOCK_BOOK         = 1;
    public const AUTHORIZATION_FRAUD_EMAIL = 2;
    public const LICENSE_PAYMENT_REQUEST   = 3;
}
