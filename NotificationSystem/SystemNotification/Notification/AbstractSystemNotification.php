<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\SystemNotification\Notification;

use App\TestTask\NotificationSystem\NotificationInterface;

abstract class AbstractSystemNotification implements NotificationInterface
{
    public function getType(): int
    {
        return static::TYPE;
    }
}
