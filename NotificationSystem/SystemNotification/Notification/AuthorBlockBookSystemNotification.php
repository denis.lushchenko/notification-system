<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\SystemNotification\Notification;

use App\TestTask\ClassesDomainLayer\Author;
use App\TestTask\ClassesDomainLayer\Book;
use App\TestTask\ClassesDomainLayer\Moderator;
use App\TestTask\NotificationSystem\ValueObject\NotificationType;

class AuthorBlockBookSystemNotification extends AbstractSystemNotification
{
    protected const TYPE = NotificationType::AUTHOR_BLOCK_BOOK;

    /** @var Moderator */
    private $moderator;

    /** @var Author */
    private $author;

    /** @var Book */
    private $book;

    /** @var string */
    private $comment;

    /** @var string */
    private $systemMessageText;

    public function __construct(Moderator $moderator, Author $author, Book $book, string $comment)
    {
        $this->moderator         = $moderator;
        $this->author            = $author;
        $this->book              = $book;
        $this->comment           = $comment;
        $this->systemMessageText = 'Some text';
    }

    public function getSystemMessageText(): string
    {
        return $this->systemMessageText;
    }

    public function getSystemRecipient(): int
    {
        return $this->author->getId();
    }

    public function getSystemData(): array
    {
        return [
            'moderatorId' => $this->moderator->getId(),
            'authorId'    => $this->author->getId(),
            'bookId'      => $this->book->getId(),
            'comment'     => $this->comment
        ];
    }

    public function getAdditionalData(): array
    {
        return [
            'moderatorId' => $this->moderator->getId(),
            'authorId'    => $this->author->getId(),
            'bookId'      => $this->book->getId(),
            'comment'     => $this->comment,
        ];
    }
}
