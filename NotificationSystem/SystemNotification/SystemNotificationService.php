<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\SystemNotification;

use App\TestTask\ClassesDomainLayer\SystemService;
use App\TestTask\NotificationSystem\AbstractNotificationService;
use App\TestTask\NotificationSystem\NotificationInterface;

class SystemNotificationService extends AbstractNotificationService
{
    /** @var SystemService */
    private $systemService;

    public function __construct(SystemService $systemService)
    {
        $this->systemService = $systemService;
    }

    public function addNotification(NotificationInterface $notification): void
    {
        $this->systemService->notify(
            $notification->getSystemMessageText(),
            $notification->getSystemRecipient(),
            $notification->getSystemData(),
            );

        $this->saveNotification($notification);
    }
}
