<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\EmailNotification;

use App\TestTask\ClassesDomainLayer\EmailService;
use App\TestTask\NotificationSystem\AbstractNotificationService;
use App\TestTask\NotificationSystem\NotificationInterface;

class EmailNotificationService extends AbstractNotificationService
{
    /** @var EmailService */
    private $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function addNotification(NotificationInterface $notification): void
    {
        $this->emailService->notify(
            $notification->getEmailRecipient(),
            $notification->getEmailTemplate(),
            $notification->getEmailData(),
            );

        $this->saveNotification($notification);
    }
}
