<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\EmailNotification\Notification;

use App\TestTask\ClassesDomainLayer\User;
use App\TestTask\NotificationSystem\ValueObject\NotificationType;

class AuthorizationFraudEmailNotification extends AbstractEmailNotification
{
    protected const TYPE = NotificationType::AUTHORIZATION_FRAUD_EMAIL;

    /** @var User */
    private $user;

    /** @var string */
    private $fraudUserIp;

    public function __construct(User $user, string $fraudUserIp)
    {
        $this->user        = $user;
        $this->fraudUserIp = $fraudUserIp;
    }

    public function getEmailRecipient(): string
    {
        return $this->user->getEmail();
    }

    public function getEmailTemplate(): string
    {
        return 'AuthorizationFraudTemplate';
    }

    public function getEmailData(): array
    {
        return [
            'userName'    => $this->user->getName(),
            'fraudUserIp' => $this->fraudUserIp,
        ];
    }

    public function getAdditionalData(): array
    {
        return [
            'userId'      => $this->user->getId(),
            'fraudUserIp' => $this->fraudUserIp,
        ];
    }
}
