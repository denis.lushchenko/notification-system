<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\EmailNotification\Notification;

use App\TestTask\ClassesDomainLayer\Author;
use App\TestTask\ClassesDomainLayer\Dto\LicensePaymentRequestDto;
use App\TestTask\ClassesDomainLayer\Moderator;
use App\TestTask\NotificationSystem\ValueObject\NotificationType;

class LicensePaymentRequestAuthorEmailNotification extends AbstractEmailNotification
{
    protected const TYPE = NotificationType::LICENSE_PAYMENT_REQUEST;

    /** @var Moderator */
    private $moderator;

    /** @var Author */
    private $author;

    /** @var LicensePaymentRequestDto */
    private $licensePaymentRequestDto;

    public function __construct(
        Moderator $moderator,
        Author $author,
        LicensePaymentRequestDto $licensePaymentRequestDto
    ) {
        $this->moderator                = $moderator;
        $this->author                   = $author;
        $this->licensePaymentRequestDto = $licensePaymentRequestDto;
    }

    public function getEmailRecipient(): string
    {
        return $this->author->getEmail();
    }

    public function getEmailTemplate(): string
    {
        return 'LicensePaymentRequestAuthorTemplate';
    }

    public function getEmailData(): array
    {
        return [
            'currency'  => $this->licensePaymentRequestDto->getCurrency(),
            'sum'       => $this->licensePaymentRequestDto->getSum(),
            'requisite' => $this->covertToString($this->licensePaymentRequestDto->getRequisite()),
        ];
    }

    public function getAdditionalData(): array
    {
        return [
            'moderatorId' => $this->moderator->getId(),
            'authorId'    => $this->author->getId(),
            'sum'         => $this->licensePaymentRequestDto->getSum(),
            'requisite'   => $this->covertToString($this->licensePaymentRequestDto->getRequisite()),
        ];
    }
}
