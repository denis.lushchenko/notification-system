<?php

declare(strict_types = 1);

namespace App\TestTask\NotificationSystem\EmailNotification\Notification;

use App\TestTask\NotificationSystem\NotificationInterface;

abstract class AbstractEmailNotification implements NotificationInterface
{
    public function getType(): int
    {
        return static::TYPE;
    }

    protected function covertToString(array $requisite): string
    {
        return implode(',', $requisite);
    }
}
